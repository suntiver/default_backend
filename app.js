require('dotenv').config()
const express =  require('express')
const morgan  = require('morgan')
const helmet   = require('helmet')
const bodyParser = require('body-parser');
const routes = require('./routes')
const dbStartup = require('./configs/database')
const cors = require('cors')
const path = require('path')
const app = express()

// CORS
const allowedOrigins = []
const corsOptions = {
    origin: function (origin, callback) {
      if (!origin) return callback(null, true)
      if (allowedOrigins.indexOf(origin) === -1) {
        const msg = 'The CORS policy for this site does not ' + 'allow access from the specified Origin.'
        return callback(new Error(msg), false)
      }
      return callback(null, true)
    },
  }

app.use(cors(corsOptions))
app.use(helmet())
app.use(express.json())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }));

// Static file
app.use('/static', express.static(path.join(__dirname, './public')))

// Custom Response Format
app.use(require('./configs/responseFormat'))


// when run code start  connect db
dbStartup()
  
if(app.get('env') === 'development'){
    // Logger
    app.use(morgan())
}


// Routes
app.use('/api',routes)

// Error handler
require('./configs/errorHandles')(process.env.NODE_ENV != "development", app)

console.log(`NODE_ENV: ${process.env.NODE_ENV}`);


module.exports = app