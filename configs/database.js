require("dotenv").config();
const mongoose = require('mongoose');
const logger = require('../configs/logger')

module.exports = function() {
     mongoose.connect(process.env.MONGODB_URL)
    .then(() => logger.info('Connected to MongoDB'))


} 