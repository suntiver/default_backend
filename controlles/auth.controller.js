require("dotenv").config();
const bcrypt = require("bcrypt");
const _ = require("lodash");
const jwt = require("jsonwebtoken");
const { validateAuth } = require("../validations/auth.validation");
const { User, validate } = require("../model/user");

let refreshTokens = [];

const methods = {
  async onLogin(req, res, next) {

      const { error } = validateAuth(req.body);
      if (error) return res.error({message:error.details[0].message,statusCode:400})
      let user = await User.findOne({ email: req.body?.email });
      if (!user) return res.error({message:"Invalid email ",statusCode:400})

      const validPassword = await bcrypt.compare(
        req.body.password,
        user.password
      );
      // if (!validPassword) return res.status(400).send("Invalid password.");
      if (!validPassword) return res.error({message:"Invalid password.",statusCode:400})

      const accessToken = jwt.sign(
        { _id: user._id, roles: user.roles[0] },
        process.env.SECRET,
        { expiresIn: process.env.TOKEN_EXP_DAY }
      );
      const refreshToken = jwt.sign(
        { _id: user._id, roles: user.roles[0] },
        process.env.REFRESH_TOKEN_SECRET,
        { expiresIn: process.env.REFRESH_EXP_TOKEN }
      );

      // Set refersh token in refreshTokens array
      refreshTokens.push(refreshToken);

      // return res.success({accessToken,refreshToken}, 200)
      return res.success({data:{accessToken,refreshToken}})
     
 
   
  },

  async userRegister(req, res, next) {
  
      const { error } = validate(req.body);
      if (error) return res.error({message:error.details[0].message,statusCode:400})

      let user = await User.findOne({ email: req.body?.email });
      if (user) return  res.error({message:"User already registered",statusCode:400})

      user = new User(_.pick(req.body, ["name", "email", "password"]));
      const salt = await bcrypt.genSalt(10);
      user.password = await bcrypt.hash(user.password, salt);
      await user.save();

      const accessToken = jwt.sign(
        { _id: user._id, roles: user.roles[0] },
        process.env.SECRET,
        { expiresIn: process.env.TOKEN_EXP_DAY }
      );

      const respone = {
        _id: user.id,
        name: user.name,
        email: user.email,
        accessToken: accessToken,
      };

      return res.success({data:respone})
   
  },

  async getToken(req, res, next) {
  
      const refreshToken =
        req.body.refreshToken ||
        req.query.refreshToken ||
        req.headers["Authorization"] ||
        req.header("Authorization");

      // If token does not exist, send error message
      if (!refreshTokens.includes(refreshToken)) {
        return  res.error({message:"Invalid refresh token",statusCode:403})
      }

      // If token is not provided, send error message
      if (refreshToken) {
        await jwt.verify(
          refreshToken,
          process.env.REFRESH_TOKEN_SECRET,
          async function (err, decoded) {
            if (err) {
              return  res.error({message:"Unauthorized access.",statusCode:401})
            }

            const { _id, roles } = decoded;
            const accessToken = await jwt.sign(
              { _id, roles },
              process.env.SECRET,
              { expiresIn: process.env.TOKEN_EXP_DAY }
            );
            const refreshToken = await jwt.sign(
              { _id, roles },
              process.env.REFRESH_TOKEN_SECRET,
              { expiresIn: process.env.REFRESH_EXP_TOKEN }
            );

            const response = {
              status: "success",
              accessToken,
              refreshToken,
            };

            return res.success({data:response})
          }
        );
      } else {
        // if there is no accessToken
        // return an error
        return  res.error({message:"No accessToken provided.",statusCode:403})
      }
    
  },

  async logOut(req, res , next) {
  
        const refreshToken =
        req.body.refreshToken ||
        req.query.refreshToken ||
        req.headers["Authorization"] ||
        req.header("Authorization");
  
      // If token does not exist, send error message
      if (!refreshTokens.includes(refreshToken)) {
          return  res.error({message:"Invalid refresh token",statusCode:403})
      }
  

      return res.success({data:"Logged out"})
 
     
  },
};

module.exports = { ...methods };
