require('dotenv').config()
const {User} = require('../model/user')
const _  = require('lodash')


const methods = {
    async userInfo(req,res,next) {
        
      
            const user = await User.findById(req.user._id).select('-password')
            res.send(user)
            
       

    },

    async userAll(req,res,next) {
       
            const user = await User.find()
            res.send(user)
            
        
  
    }

}

module.exports = { ...methods }