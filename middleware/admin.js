function admin(req,res,next) {
    //401 Unauthorized
    //403 Forbidden
    if(req.user.roles[0] != 'admin') return res.status(403).send('Access denied')
    next()
    
}

module.exports = admin