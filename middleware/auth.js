require('dotenv').config()
const jwt = require('jsonwebtoken')


function auth(req,res,next) {
    const accessToken =  req.body.accessToken || req.query.accessToken || req.headers['accessToken'] || req.header('accessToken') 
     // decode token
    if (accessToken) {
        // verifies secret and checks exp
        jwt.verify(accessToken, process.env.SECRET, function(err, decoded) {
            if (err ) {
                return res.status(401).json({"error": true, "message": 'Unauthorized access.' });
            }
            req.user = decoded;
            next()
        });
        
    } else {
        // if there is no accessToken
        // return an error
        return res.status(403).send({
            "error": true,
            "message": 'No accessToken provided.'
        });
    }
    
}

module.exports = auth