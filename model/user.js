require('dotenv').config()
const Joi = require('joi')
const passwordComplexity = require("joi-password-complexity");
const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    name:{
        type:String,
        required:true,
        minlength:5,
        maxlength:50
    },
    email:{
        type:String,
        required:true,
        minlength:5,
        maxlength:255,
        unique:true
    },
    password:{
        type:String,
        required:true,
        minlength:5,
        maxlength:1024
    },
    roles: {
        type: [{
            type: String,
            enum: ['user', 'admin']
        }],
        default: ['user']
    },
    updated: {
        type: Date
    },
    created: {
        type: Date,
        default: Date.now
    },
})



const User = mongoose.model('User',userSchema)

function validateUser(user) {

    const complexityOptions = {
        min: 10,
        max: 30,
        lowerCase: 1,
        upperCase: 1,
        numeric: 1,
        symbol: 1,
        requirementCount: 2,
      };

    const schema = Joi.object({
        name: Joi.string().min(5).required(),
        email:Joi.string().min(5).max(255).required().email(),
        password: passwordComplexity(complexityOptions).required()

    })

    return schema.validate(user);
    
}

exports.User =  User;
exports.validate = validateUser;