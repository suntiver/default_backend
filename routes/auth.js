const router = require('express').Router()
const controllers = require('../controlles/auth.controller')


router.post('/login' ,controllers.onLogin)
router.post('/register' ,controllers.userRegister)
router.post('/token' ,controllers.getToken)
router.delete('/logout' ,controllers.logOut)

module.exports = router

