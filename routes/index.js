require('dotenv').config()
const router = require('express').Router()

router.use('/auth',require('./auth'))
router.use('/movie',require('./movie'))
router.use('/',require('./users'))

module.exports = router