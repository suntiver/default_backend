const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth')
const admin = require('../middleware/admin')
const controllers = require('../controlles/movie.controller')

router.get('/',[auth,admin],controllers.getMovieAll);


module.exports = router