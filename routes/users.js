const router = require('express').Router()
const auth = require('../middleware/auth')
const controllers = require('../controlles/user.controller')

router.get('/me',auth,controllers.userInfo)
router.get('/users',auth,controllers.userAll)

module.exports = router