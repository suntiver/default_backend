// let server
let request = require("supertest");
let app;
describe("/api/auth", () => {
  beforeEach(() => {
    app = require("../../app");
  });

  describe("login", () => {
    it("should return 200 if login success  ", async () => {
      await request(app)
        .post("/api/auth/login")
        .send({
          email: "suntiver1@live.com",
          password: "01a02b03c04d05e",
        })
        .then((res) => {
          expect(res.status).toBe(200);
        });
    });

    it("should return 400 if forgot  email  passed in body ", async () => {
      await request(app)
        .post("/api/auth/login")
        .send({})
        .then((res) => {
          expect(res.status).toBe(400);
        });
    });

    it("should return 400 if invalid email  is passed ", async () => {
      await request(app)
        .post("/api/auth/login")
        .send({
          email: "suntiv@live.com",
          password: "01a02b03c04d05e",
        })
        .then((res) => {
          expect(res.status).toBe(400);
        });
    });

    it("should return 400 if invalid password  is passed ", async () => {
      await request(app)
        .post("/api/auth/login")
        .send({
          email: "suntiver1@live.com",
          password: "0102b03c04d05e",
        })
        .then((res) => {
          expect(res.status).toBe(400);
        });
    });


  });

  describe('register', () => {
    
  })
  
  
});
