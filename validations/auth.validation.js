const Joi = require('joi')

function validateAuth(req) {

    const schema = Joi.object({
        email:Joi.string().required().email(),
        password: Joi.string().required(),
    })

    return schema.validate(req);
    
}


exports.validateAuth = validateAuth;